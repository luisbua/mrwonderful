﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class PangManager : MonoBehaviour
{
    [SerializeField] private MiniGameInformationSender m_levelRecommendedSender;
    //public GameObject[] Character;
    public GameObject Player;
    public AudioClip m_pangAudioClip;

    public HealthBar healthBar;
    public Button BatteryButton;
    public Button BombButton;
    public Button GemButton;
    public Text BatteryText;
    public float TimeBattery = 5f;
    

    public bool BatteryBoost = false;
    public bool BombBoost = false;
    public bool GemBoost = false;
    public bool StopTimer = false;
    public MinigameCountdown Countdown;

    [HideInInspector] public int CountDown = 3;
    public int ElapsedTimeRace = 0;
    [HideInInspector] public bool bIsShieldEnable;
    public int Score;
    [HideInInspector] public int NumBallScene;
    [HideInInspector] public bool bMiniGameEnd;
    [HideInInspector] public int BatteryTimeCount;
    [HideInInspector] public bool hasLost;
    public AudioSource m_mainAudioSource;
    public AudioSource m_sfxAudioSource;

    private float RaceStartTime = 0;
    private float CountDownTime = 0;
    private bool bStartTimer = false;
    private bool bCountDownTimer = false;
    private bool bStartGame = true;
    private bool isEndVideoCountDown = false;
    private float timePaused;
    private bool _win;

    private PangCharMov pangCharMov;

    // Bomb power up
    [HideInInspector]public bool isBattery;

    // - Stun effect stars
    public GameObject stunStarsPrefab;

    // - Particle bullet effect
    public GameObject bulletHitPrefab;

    [Header("Booster particles")]
    public GameObject activateShield;
    public GameObject destroyShieldDissolve;

    // - XP Points balls
    public Text pointsUI;
    public Camera cam;

    private int FinalXP = 0;
    private float MaxTimeRaceSec = 60;

    private GameManagerMWF gm;
    private CanvasManager canvasManager;
    private PangCharMov charMove;
    private TurnDeviceAdvisor turnDeviceAdvisor;

    // - Test
    public int damageReceived;
    public int possibleTotalScore;

    public MiscSounds m_sounds;

     [System.Serializable]
     public struct MiscSounds
    {
        public AudioClip m_shieldActivate;
        public AudioClip m_shieldBroken;
        public AudioClip m_bombConsume;
        public AudioClip m_bombExplode;
    }

    public void Initialize()
    {
        FinalXP = 0;
        gm = FindObjectOfType<GameManagerMWF>();
        canvasManager = FindObjectOfType<CanvasManager>();


        gm.bIsMinGameActive = false;

        //simulo la carga desde servidor
        //gm.BoostPang.bIsBattery = BatteryBoost;
        //gm.BoostPang.bIsBomb = BombBoost;
        //gm.BoostPang.bIsGem = GemBoost;

        BatteryButton.gameObject.SetActive(gm.BoostPang.bIsBattery);
        BombButton.gameObject.SetActive(gm.BoostPang.bIsBomb);
        GemButton.gameObject.SetActive(gm.BoostPang.bIsGem);

        bIsShieldEnable = false;
        bMiniGameEnd = false;

        ///Instance Player
        GameObject go;
        int index = (int)gm.Player;
        go = Instantiate(gm.PlayersInfo[index].Character, Player.transform.position, Quaternion.Euler(0f,180f,0f)) as GameObject;
        go.transform.parent = Player.transform;
        charMove = FindObjectOfType<PangCharMov>();
        GetComponent<MoveParsing>().enabled = true;

        GetNumBallsScene();


        turnDeviceAdvisor = FindObjectOfType<TurnDeviceAdvisor>();
        turnDeviceAdvisor.OnDeviceOrientationValidated += Countdown.Play;
        turnDeviceAdvisor.Show();
        pangCharMov = FindObjectOfType<PangCharMov>();
        Countdown.SubscribeToCountdownFinishedEvent(EndVideo);

        m_levelRecommendedSender.SendRecommendedLevel();
        
    }


    public void FadeStartSound()
    {
        m_mainAudioSource = GetComponent<AudioSource>();
        FindObjectOfType<SoundNotDestroy>().PauseSoundWithFade(3);
        m_mainAudioSource.clip = m_pangAudioClip;
    }

    // Update is called once per frame
    void Update()
    {

        if (bStartGame)
        {
            if (!bCountDownTimer)
            {
                CountDownTime = Time.time;
                bCountDownTimer = true;
            }
            if (!isEndVideoCountDown)
            {
                //CountDown = Mathf.RoundToInt(3 - (Time.time - CountDownTime));
            }
            else
            {
                if (!bStartTimer)
                {
                    //Debug.Log("Dentro Comienzo");
                    RaceStartTime = Time.time;
                    bStartTimer = true;
                    gm.bIsMinGameActive = true;
                    charMove.bIsEnableInput = true;
                    var balls = FindObjectsOfType<BallBouncer>();
                    foreach (BallBouncer item in balls)
                    {
                        item.InitGame();
                        item.bStartGame = true;
                    }
                }
                if (!StopTimer)
                {
                    ElapsedTimeRace = Mathf.RoundToInt(Time.time - RaceStartTime - timePaused);
                }
                else
                {
                    timePaused += Time.deltaTime;
                }
            }

            BatteryBoosterBehaviour();
        }
    }

    public void CalculateXP()
    {
        m_mainAudioSource.DOFade(0, 1);
        PangCanvas pangCanvas = FindObjectOfType<PangCanvas>();
        pangCanvas.m_fadeImage.gameObject.SetActive(true);
        pangCanvas.m_fadeImage.DOFade(1, 1.2f).onComplete = delegate
        {
            int finalXP = 0;
            GameManagerMWF.Trophy lastTrophyObtained = GameManagerMWF.Trophy.Empty;
            if (!_win)
            {
                float maxXP = gm.DataXP[gm.GameLevel - 1].MaxXP;
                float minXP = gm.DataXP[gm.GameLevel - 1].MinXP;
                float percentage20 = maxXP * 0.2f;

                // - LA FORMULA // <-- Torrent, no lleva acento...
                //int timerScore = Mathf.RoundToInt(percentage20 - ElapsedTimeRace);
                int timerScore = Mathf.RoundToInt(CalculatePercentage(20) / ElapsedTimeRace) * 10;
                //int lifeScore = Mathf.RoundToInt((100 - damageReceived) * (percentage20 / 100));
                int lifeScore = CalculateLifePercentage(m_initialCharLife - damageReceived);
                int pointsScore = Mathf.RoundToInt((Score * percentage20) / possibleTotalScore);
                if (lifeScore < 0) lifeScore = 0;
                finalXP = Mathf.RoundToInt(minXP + timerScore + lifeScore + pointsScore);
                FinalXP = finalXP;

                gm.BaseScore = (int)minXP;
                gm.TimeScore = timerScore;
                gm.PointsScore = pointsScore;
                gm.LifeScore = lifeScore;
                gm.TotalXpScore = finalXP;

                if (finalXP >= gm.DataXP[gm.GameLevel - 1].CopperTrophy)
                {
                    gm.SpriteLastTrophyObtained = gm.CopperThrophy;
                    lastTrophyObtained = GameManagerMWF.Trophy.Copper;
                }
                if (finalXP >= gm.DataXP[gm.GameLevel - 1].SilverTrophy)
                {
                    gm.SpriteLastTrophyObtained = gm.SilverThrophy;
                    lastTrophyObtained = GameManagerMWF.Trophy.Silver;
                }
                if (finalXP >= gm.DataXP[gm.GameLevel - 1].GoldTrophy)
                {
                    gm.SpriteLastTrophyObtained = gm.GoldThrophy;
                    lastTrophyObtained = GameManagerMWF.Trophy.Gold;
                }

                DOTween.KillAll();
                SceneManager.LoadScene("EndMiniGame", LoadSceneMode.Single);
            }
            else
            {
                float maxXP = gm.DataXP[gm.GameLevel - 1].MaxXP;
                float minXP = gm.DataXP[gm.GameLevel - 1].MinXP;
                float percentage20 = maxXP * 0.2f;

                // - LA FORMULA
                int timerScore = Mathf.RoundToInt(CalculatePercentage(20) / ElapsedTimeRace) * 10;
                //int lifeScore = Mathf.RoundToInt((100 - damageReceived) * (percentage20 / 100));
                int lifeScore = CalculateLifePercentage(m_initialCharLife - damageReceived);
                int pointsScore = Mathf.RoundToInt((Score * percentage20) / possibleTotalScore);
                if (lifeScore < 0) lifeScore = 0;
                finalXP = Mathf.RoundToInt(minXP + timerScore + lifeScore + pointsScore);
                FinalXP = finalXP;

                gm.BaseScore = (int)minXP;
                gm.TimeScore = timerScore;
                gm.PointsScore = pointsScore;
                gm.LifeScore = lifeScore;
                gm.TotalXpScore = finalXP;

                if (finalXP >= gm.DataXP[gm.GameLevel - 1].CopperTrophy)
                {
                    gm.SpriteLastTrophyObtained = gm.CopperThrophy;
                    lastTrophyObtained = GameManagerMWF.Trophy.Copper;
                }
                if (finalXP >= gm.DataXP[gm.GameLevel - 1].SilverTrophy)
                {
                    gm.SpriteLastTrophyObtained = gm.SilverThrophy;
                    lastTrophyObtained = GameManagerMWF.Trophy.Silver;
                }
                if (finalXP >= gm.DataXP[gm.GameLevel - 1].GoldTrophy)
                {
                    gm.SpriteLastTrophyObtained = gm.GoldThrophy;
                    lastTrophyObtained = GameManagerMWF.Trophy.Gold;
                }
                DOTween.KillAll();
                SceneManager.LoadScene("WinPang", LoadSceneMode.Single);
            }
            int playerIndex = (int) gm.Player;
            gm.PlayersInfo[playerIndex].XpGained += FinalXP;
            GameManagerMWF.DataLevel level = gm.LevelsInfo[(int) gm.Game]
                .DataInfo[gm.GameLevel - 1];
            GameManagerMWF.Trophy minigameTrophyRecord = level.TrophyObtained;
            if ((int) minigameTrophyRecord < (int) lastTrophyObtained)
            {
                gm.LevelsInfo[(int) gm.Game]
                    .DataInfo[gm.GameLevel - 1].TrophyObtained = lastTrophyObtained;
            }
        };
    }

    int CalculatePercentage(float value)
    {
        float baseMax = Mathf.RoundToInt(gm.DataXP[gm.GameLevel - 1].MaxXP - gm.DataXP[gm.GameLevel - 1].MinXP);
        baseMax = (baseMax * value) / 100;
        return Mathf.RoundToInt(baseMax);
    }

    public void BatteryButtonPressed()
    {
        if(bStartGame)
        {
            charMove.MaxNumShoots = 2;
            BatteryText.gameObject.SetActive(true);
            StartCoroutine(StartCountDown((int)TimeBattery, 0));
            Invoke("ResetBattery", TimeBattery);
        }
    }
    public void NewBatteryButtonPressed()
    {
        Camera mainCamera = Camera.main;
        if (bStartGame)
        {
            isBattery = true;
            FindObjectOfType<PangCanvas>().m_batteryButton.gameObject.SetActive(false);
            Tween cameraTween = mainCamera.transform.DOShakePosition(TimeBattery, strength: 0.1f, vibrato: 20, fadeOut: false);
            cameraTween.onUpdate = delegate
            {
                if (hasLost)
                {
                    cameraTween.Kill();
                }
            };
            Invoke("NewResetBattery", TimeBattery);

            gm.PowerupInfo[3].UnitsLeft--;
        }
    }
    void BatteryBoosterBehaviour()
    {
        if (hasLost)
        {
            return;
        }
        if (isBattery)
        {
            pangCharMov.ConstantFire();
            print("Disparando");
        }
        else
        {
            pangCharMov.gameObject.GetComponent<Animator>().SetBool("ConstantShoot", false);
        }
    }

    public void BombButtonPressed()
    {
        if (!bStartGame)
        {
            return;
        }
        Debug.Log("Pulsado boton bomba");

        gm.PowerupInfo[4].UnitsLeft--;

        var bombAnimation = FindObjectOfType<AbstractAnimation>();
        bombAnimation.OnAnimationStarted += delegate
        {
            FindObjectOfType<PangPauser>().Pause();
            SingletonSoundPlayer.Instance.m_audioSource.PlayOneShot(m_sounds.m_bombConsume);
        };
        bombAnimation.OnAnimationFinished += delegate
        {
            FindObjectOfType<PangPauser>().Resume();
            Camera.main.transform.DOShakePosition(1f, strength: 0.4f, vibrato: 25, fadeOut: true);
            var balls = FindObjectsOfType<BallSplit>();
            FindObjectOfType<PangCanvas>().m_bombButton.gameObject.SetActive(false);
            //m_sfxAudioSource.PlayOneShot(m_sounds.m_bombExplode);
            SingletonSoundPlayer.Instance.m_audioSource.PlayOneShot(m_sounds.m_bombExplode);
            foreach (BallSplit item in balls)
            {
                if(item.SizeBall == BallBouncer.SizeBall.Large)
                {
                    item.Split(true);
                }
            }
            Invoke("BombSplit2", 1);
        };
        bombAnimation.Animate();
    }

    public void GemButtonPressed()
    {
        if(bStartGame)
        {
            bIsShieldEnable = true;
            Player.transform.GetChild(0).Find("Shield").gameObject.SetActive(true);
            FindObjectOfType<PangCanvas>().m_gemButton.gameObject.SetActive(false);

            gm.PowerupInfo[5].UnitsLeft--;
        }
    }

    void ResetBattery()
    {
        charMove.MaxNumShoots = 1;
        BatteryText.gameObject.SetActive(true);
    }
    void NewResetBattery()
    {
        isBattery = false;
    }

    void BombSplit2()
    {
        Camera.main.transform.DOShakePosition(1f, strength: 0.4f, vibrato: 25, fadeOut: true);
        var balls2 = FindObjectsOfType<BallSplit>();
        //Debug.Log("bolas2: " + balls2.Length);
        foreach (BallSplit item2 in balls2)
        {
            if (item2.SizeBall == BallBouncer.SizeBall.Medium)
            {
                item2.Split(true);
            }
        }
    }

    public void GetNumBallsScene()
    {
        Invoke("BallsNextFrame", 0.1f);
    }

    private void BallsNextFrame()
    {
        GameObject[] go = GameObject.FindGameObjectsWithTag("Ball");
        NumBallScene = go.Length;
        if(NumBallScene <= 0)
        {
            EndGameByWin();
        }
    }

    public void EndGameByWin()
    {
        _win = true;

        gm.pangLevelWon = SceneManager.GetActiveScene().name;
        bMiniGameEnd = true;
        gm.bIsMinGameActive = false;
        charMove.bIsEnableInput = false;
        StopTimer = true;
        FindObjectOfType<PangCanvas>().m_fadeImage.gameObject.SetActive(true);
        FindObjectOfType<PangCanvas>().m_fadeImage.DOFade(1, 1f).onComplete = delegate
        {
            //CalculateXP();
            m_pangExperienceCalculator.UpdateValues();
        };
        print("Has ganado");

        //FindObjectOfType<MiniGameInformationSender>().SendRecommendedLevel();
    }
    private AExperienceCalculator m_pangExperienceCalculator;
    public void SetExperienceCalculator(AExperienceCalculator calculator)
    {
        m_pangExperienceCalculator = calculator;
    }
    public void EndGameByLost()
    {
        _win = false;
        gm.pangLevelWon = SceneManager.GetActiveScene().name;
        FindObjectOfType<PangPauser>().Lost();
        bMiniGameEnd = true;
        gm.bIsMinGameActive = false;
        charMove.bIsEnableInput = false;
        StopTimer = true;
        m_mainAudioSource.DOFade(0, 2);
    }

    public void LoadMenu()
    {
        SceneLoadersContainer.Instance.GetSceneLoader("MainSceneLoader").LoadSceneAsync("Mapamundi");
    }

    IEnumerator StartCountDown(int count, int OptionBoost)
    {
        for (int i = 0; i < count; i++)
        {
            switch (OptionBoost)
            {
                case 0:
                    BatteryTimeCount = count - i;
                    break;
                case 1:
                    break;
                case 2:
                    break;
                default:
                    break;
            }
            yield return new WaitForSeconds(1f);
        }
    }


    public void EndVideo()
    {
        isEndVideoCountDown = true;
        m_mainAudioSource.Play();
        m_mainAudioSource.DOFade(0.7f, 0.5f);
        m_initialCharLife = FindObjectOfType<CharacterStats>().Life;

    }

    public void DestroyWithDelay(float delay, GameObject gameObject)
    {
        Destroy(gameObject, delay);
    }
    private int m_initialCharLife;
    private int CalculateLifePercentage(int value)
    {
        int maxLife = m_initialCharLife;
        int returnValue = (Mathf.Abs(value) * 100) / maxLife;
        return returnValue;
    }

}
