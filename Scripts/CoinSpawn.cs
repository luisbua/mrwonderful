﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinSpawn : MonoBehaviour
{
    public GameObject Coin;
    public SectionsSpawn sectionSpawn;
    public Obstacles obstacles;
    public float DistanceBetweenCoins = 2f;

    private int numCoins;
    private List<int> shuffleIndex;
    private RunExperienceCalculator m_calculateMinigameXP;

    // Start is called before the first frame update
    void Start()
    {

        sectionSpawn.CreatedSection += SpawnSection;
        shuffleIndex = new List<int>();

        m_calculateMinigameXP = FindObjectOfType<RunExperienceCalculator>();
    }


    void SpawnSection(int HolyType)
    {
        ShuffleArray();
        SpawnCoins();
    }

    void SpawnCoins()
    {
        numCoins = 5;
        int numPositions = Random.Range(0, 5);
        for (int j = 0; j < numPositions; j++)
        {
            int matpos = shuffleIndex[j];
            Vector3 pos = sectionSpawn.PositionMatrix[matpos].pos;
            
            for (int i = 0; i < numCoins; i++)
            {
                pos.z = sectionSpawn.PositionMatrix[matpos].pos.z + (i * DistanceBetweenCoins);
                pos.y = 0.25f;
                GameObject go;
                go = Instantiate(Coin, pos, Quaternion.identity);
                m_calculateMinigameXP.AddInstantiatedCoin();
                go.transform.parent = sectionSpawn.sections[sectionSpawn.sections.Count - 1].transform;
            }
        }
    }
    void ShuffleArray()
    {
        ///Create new List
        shuffleIndex.Clear();
        for (int i = 0; i < sectionSpawn.PositionMatrix.Length; i++)
        {
            if (sectionSpawn.PositionMatrix[i].bIsEmpty)
            {
                shuffleIndex.Add(i);
            }
        }
        ///Shuffle created List
        for (int i = 0; i < shuffleIndex.Count; i++)
        {
            int temp = shuffleIndex[i];
            int randomIndex = Random.Range(i, shuffleIndex.Count);
            shuffleIndex[i] = shuffleIndex[randomIndex];
            shuffleIndex[randomIndex] = temp;
        }
    }
}
