﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PangCharMov : MonoBehaviour
{
    public GameObject Projectile;
    [HideInInspector] public bool bIsMovingLeft = false;
    [HideInInspector] public bool bIsMovingRight = false;
    [HideInInspector] public bool bIsEnableInput = false;
    [HideInInspector] public int MaxNumShoots;
    [HideInInspector] public int ActualNumShoots = 0;
    //[HideInInspector] public bool isStunned = false;


    private GameManagerMWF gm;
    /// Luego seran privates
    private int PlayerSpeed = 10;
    private int PlayerAttack = 10;
    private int PlayerTough = 10;
    private Vector2 fingerDownPosition;
    private Vector2 fingerUpPosition;
    private float TimeTap;
    private bool bIsShoot = false;
    public float RealSpeed = 0;
    public float RealAttack = 0;

    private Tween m_colorTween;
    private Color m_StartMaterialColor;

    // - Variable creada para referencias (Anthony)
    //public float refSpeed;
    //public float refAtack;

    // - Temporizador para IDLES aleatorios
    private float timer = 3;


    // - Booleana para evitar ser tocado de nuevo


    // Start is called before the first frame update
    void Start()
    {
        gm = FindObjectOfType<GameManagerMWF>();

        ///Descomentar una vez las stats esten bien puestas
        PlayerSpeed = gm.PlayersInfo[(int)gm.Player].RealSpeed;
        PlayerAttack = gm.PlayersInfo[(int)gm.Player].RealAttack;
        PlayerTough = gm.PlayersInfo[(int)gm.Player].RealToughness;

        //RealSpeed = 3 + Mathf.Log10(PlayerSpeed * 0.1f) * 5; -- Linea original creada por Luis (modif. 03/05/2021)
        //RealAttack = 15 + ((Mathf.Log10(PlayerAttack * 0.1f) * 43.47f)); -- Linea original creada por Luis (modif. 03/05/2021)

        MaxNumShoots = 1;
        ActualNumShoots = 0;

        m_StartMaterialColor = GetComponentInChildren<Renderer>().material.color;

        GetComponentInChildren<Renderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;

    }
    void ResetStun()
    {
        if (!FindObjectOfType<PangManager>().hasLost)
        {
            bIsEnableInput = true;
            Color color = new Color(m_StartMaterialColor.r, m_StartMaterialColor.g, m_StartMaterialColor.b, 0);             
            Tween myTween = GetComponentInChildren<Renderer>().material.DOColor(color, 0.09f).SetLoops(10, LoopType.Yoyo);
            myTween.onComplete = delegate
            {
                Color color02 = new Color(m_StartMaterialColor.r, m_StartMaterialColor.g, m_StartMaterialColor.b, 1);
                GetComponent<Rigidbody>().isKinematic = false;
                GetComponent<Collider>().enabled = true;
            };
        }
    }

    // Update is called once per frame
    void Update()
    {
        foreach (Touch touch in Input.touches)
        {
            if (touch.phase == TouchPhase.Began)
            {
                fingerUpPosition = touch.position;
                fingerDownPosition = touch.position;
                TimeTap = Time.time;
                ///Texto de debug en Movil
                FindObjectOfType<CanvasManager>().debugText.text = "Toco: " + touch.position.ToString();
            }

            if (touch.phase == TouchPhase.Ended)
            {
                fingerUpPosition = touch.position;
                TimeTap = Time.time - TimeTap;

                ///Delimito a zona de touch de la pantalla (no esta bien delimitada
                if(fingerUpPosition.y > 850)
                {
                        bIsShoot = (TimeTap < 0.3) ? true : false;
                        FindObjectOfType<CanvasManager>().debugText.text = "Disparo 1 ";
                }
                else 
                {
                    if(fingerUpPosition.x > 300 && fingerUpPosition.x < 800 && fingerUpPosition.y > 560)
                    {
                        bIsShoot = (TimeTap < 0.3) ? true : false;
                        FindObjectOfType<CanvasManager>().debugText.text = "Disparo 2";
                    }
                }
            }

            if (touch.phase == TouchPhase.Moved)
            {
                //fingerUpPosition = touch.position;
            }
        }
        RandomIdle();

        //////SOLO PARA TEST
        //if (Input.GetKey(KeyCode.A) || bIsMovingLeft)
        //{
        //    MoveLeft();
        //}
        //if (Input.GetKey(KeyCode.D) || bIsMovingRight)
        //{
        //    MoveRight();
        //}
        //if(bIsShoot || Input.GetKeyDown(KeyCode.E))
        //{
        //    Fire();
        //    bIsShoot = false;
        //}

        //if(transform.position.x <= -4.406121f || transform.position.x >= 7.424925f)
        //{
        //    FindObjectOfType<MoveParsing>().limitPosition = true;
        //}
        //else
        //{
        //    FindObjectOfType<MoveParsing>().limitPosition = false;
        //}

        //if (isStunned)
        //{
        //    StunBehaviourByDamage();
        //}
    }

    void StunBehaviourByDamage()
    {
        //Invoke("ResetStun", 1);
        bIsEnableInput = false;
        GetComponent<Rigidbody>().isKinematic = true;
        GetComponent<Collider>().enabled = false;
        

    }
    [SerializeField] private AudioClip m_shootAudioClip;
    public void Fire()
    {
        if(bIsEnableInput && !FindObjectOfType<PangManager>().isBattery)
        {
            if(ActualNumShoots < MaxNumShoots)
            {
                SingletonSoundPlayer.Instance.m_audioSource.PlayOneShot(m_shootAudioClip, 0.15f);
                Vector3 pos = transform.position + new Vector3(0, 2.0f, 0);
                //GameObject go = Instantiate(Projectile, pos, Quaternion.identity);
                GameObject go = Instantiate(Projectile, pos, Projectile.transform.rotation);
                go.GetComponent<Rigidbody>().AddForce(0f, RealAttack, 0f, ForceMode.Impulse);
                ActualNumShoots++;
                //Debug.Log("Numero de Disparos: " + ActualNumShoots + " Numero Maximo: " + MaxNumShoots);
                GetComponent<Animator>().SetTrigger("Shoot_Up");
            }
        }
    }
    private bool canShoot = true;
    private bool shooRight = true;
    public void ConstantFire()
    {
        FindObjectOfType<Animator>().SetBool("ConstantShoot", true);
        float delayTime = 0.2f;
        if (canShoot)
        {
            Vector3 pos01 = transform.position + new Vector3(-0.3f, 2, 0);
            Vector3 pos02 = transform.position + new Vector3(0.3f, 2, 0);
            if (shooRight)
            {
                GameObject go = Instantiate(Projectile, pos01, Projectile.transform.rotation);
                go.GetComponent<Rigidbody>().AddForce(0f, RealAttack, 0f, ForceMode.Impulse);
            }
            else
            {
                GameObject go = Instantiate(Projectile, pos02, Projectile.transform.rotation);
                go.GetComponent<Rigidbody>().AddForce(0f, RealAttack, 0f, ForceMode.Impulse);
            }
            canShoot = false;
            SingletonSoundPlayer.Instance.m_audioSource.PlayOneShot(m_shootAudioClip, 0.15f);

            StartCoroutine(ConstanteFireCr(delayTime));
        }
    }
    IEnumerator ConstanteFireCr(float delay)
    {
        yield return new WaitForSeconds(delay);
        canShoot = true;
        if (shooRight)
        {
            shooRight = false;
        }
        else
        {
            shooRight = true;
        }
    }

    /// <summary>
    /// /SOLO PARA TEST
    /// </summary>
    //public void MoveLeft()
    //{
    //    if (bIsEnableInput)
    //    {
    //        transform.position = new Vector3(transform.position.x - (RealSpeed * Time.deltaTime), 0.2f, 0);
    //    }
    //}

    ///// <summary>
    ///// SOLO PARA TEST
    ///// </summary>
    //public void MoveRight()
    //{
    //    if (bIsEnableInput)
    //    {
    //        transform.position = new Vector3(transform.position.x + (RealSpeed * Time.deltaTime), 0.2f, 0);
    //    }
    //}

    public void Movement(float value)
    {
        if (bIsEnableInput)
        {
            //transform.position = new Vector3(transform.position.x + (RealSpeed * Time.deltaTime * value), 0.2f, 0);
            //transform.Translate(RealSpeed * value * Time.deltaTime, 0, 0); // - Movimiento muy repentino.

            Vector3 newPosition = transform.position + new Vector3(value, 0, 0);
            transform.position = Vector3.Lerp(transform.position, newPosition, RealSpeed * Time.deltaTime);


            // - Limitación de movimiento en limites.
            Vector3 clampedPosition = transform.position;
            clampedPosition.x = Mathf.Clamp(clampedPosition.x, -5.5f, 5.5f);
            transform.position = clampedPosition;

            Animator anim = GetComponent<Animator>();
            if (value != 0)
            {
                anim.SetTrigger("Moving_Lateral");
                anim.SetFloat("PangMoveDirection", value);
                if (value == 1)
                {
                    gameObject.transform.rotation = Quaternion.Euler(0, -180, 0);
                }
                else
                {
                    gameObject.transform.rotation = Quaternion.Euler(-180, 0, -180);
                }
            }
            else
            {
                anim.SetTrigger("Idle");
            }
            //Debug.Log(value);
            
        }
    }

    public void NewMovement(float value)
    {
        if (bIsEnableInput)
        {
            Vector3 newPosition = transform.position + new Vector3(value, 0, 0);
            transform.position = Vector3.Lerp(transform.position, newPosition, RealSpeed * Time.deltaTime);

            // - Limitación de movimiento en limites.
            Vector3 clampedPosition = transform.position;
            clampedPosition.x = Mathf.Clamp(clampedPosition.x, -5.5f, 5.5f);
            transform.position = clampedPosition;

            // - Animation
            Animator anim = GetComponent<Animator>();
            if (value == 0)
            {
                anim.SetTrigger("Idle");
                transform.DOLocalRotate(new Vector3(0, 180, 0), 0.1f);
            }
            else
            {
                anim.SetTrigger("Run");
                if (value == 1)
                {
                    transform.DOLocalRotate(new Vector3(0, 115, 0), 0.1f);
                }
                else if (value == -1)
                {
                    transform.DOLocalRotate(new Vector3(0, -115, 0), 0.1f);
                }
            }
        }
        
    }

    void RandomIdle() // - Barajar la posibilidad de implementarlo en donde afecte a todos
    {
        Animator anim = GetComponent<Animator>();
        if (anim.GetCurrentAnimatorStateInfo(0).IsName("Idle_1"))
        {
            timer-=Time.deltaTime;
            if (timer <= 0)
            {
                timer = Random.Range(2, 5);
                anim.SetInteger("Idle_int", Random.Range(1, 5));
            }
        }
        
    }

    public void ReceiveDamage()
    {
        bIsEnableInput = false;
        GetComponent<Animator>().SetTrigger("Idle");
        GetComponent<Rigidbody>().isKinematic = true;
        GetComponent<Collider>().enabled = false;
        m_colorTween.Kill();
        m_colorTween = GetComponentInChildren<Renderer>().material.DOColor(Color.red, 0.05f);
        m_colorTween.SetLoops(8, LoopType.Yoyo);
        m_colorTween.onComplete = delegate
        {
            ResetStun();
        };
    }
        
}
