﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    [Header("Dependencies")]
    [SerializeField] private Gradient m_gradient;
    [SerializeField] private Image m_fill;
    [SerializeField] private Image m_fillBack;
    [SerializeField] private Image m_heart;
    [SerializeField] private Image m_bar;

    [Header("Health Fill Animation")]
    [SerializeField] private float m_durationHealthFillAnimation = 0.3f;
    [SerializeField] private float m_delayFillBackHealthFillAnimation = 0.5f;

    [Header("Shake Health Bar Animation")]
    [SerializeField] private float m_durationShakeHealthBarAnimation = 0.5f;
    [SerializeField] private float m_strengthShakeHealthBarAnimation = 10f;
    [SerializeField] private int m_vibratoShakeHealthBarAnimation = 20;

    [Header("Heart And Bar Colors Animation")]
    [SerializeField, Range(0, 100)] private float m_lowHealthPercentageThreshold = 100f/3f;
    [SerializeField] private float m_durationHeartAndBarColorsAnimation = 0.4f;
    [SerializeField] private Color m_colorHeartAndBarColorsAnimation = Color.red;

    private RectTransform m_rectTransform;
    private float m_healthFillUi;
    private float m_healthPercentage;
    private float m_fillMinimumSizeX;
    private float m_fillMaximumSizeX;
    private int m_minimumHealth;
    private int m_maximumHealth;
    private bool m_lowStateActive;
    private Tween m_fillAnimation;
    private Tween m_fillBackAnimation;
    private Tween m_shakeHealthBarAnimation;
    private Tween m_heartColorAnimation;
    private Tween m_barColorAnimation;

    private void Start()
    {
        m_rectTransform = GetComponent<RectTransform>();
        m_fillMinimumSizeX = 0;
        m_fillMaximumSizeX = m_fill.rectTransform.sizeDelta.x;
    }

    public void SetValues(int max, int min)
    {
        m_minimumHealth = min;
        m_maximumHealth = max;
        m_fill.color = m_gradient.Evaluate(1f);
    }

    public void SetHealth(int health)
    {
        SetHealthValues(health);
        KillAllAnimationsDependingOnHealthPercentage();
        AnimateHealthFill();
        ShakeHealthBar();
        AnimateHeartAndBarColors();
    }

    private void SetHealthValues(int health)
    {
        m_healthFillUi = ConvertHealthToHealthFillUi(health);
        m_healthPercentage = ConvertHealthToHealthPercentage(health);
    }

    private void KillAllAnimationsDependingOnHealthPercentage()
    {
        m_fillAnimation.Kill(true);
        m_fillBackAnimation.Kill(true);
        m_shakeHealthBarAnimation.Kill(true);
        if (!HealthIsLow())
        {
            m_heartColorAnimation.Kill(true);
            m_barColorAnimation.Kill(true);
        }
        else
        {
            m_heart.color = Color.white;
            m_bar.color = Color.white;
        }
    }

    private float ConvertHealthToHealthFillUi(int health)
    {
        return (float) (health - m_minimumHealth) / (m_maximumHealth - m_minimumHealth) *
            (m_fillMaximumSizeX - m_fillMinimumSizeX) + m_fillMinimumSizeX;
    }

    private float ConvertHealthToHealthPercentage(int health)
    {
        return 100f * ((float) (health - m_minimumHealth) / (m_maximumHealth - m_minimumHealth));
    }

    private void AnimateHealthFill()
    {
        m_fillAnimation = m_fill.rectTransform.DOSizeDelta(new Vector2(m_healthFillUi, m_fill.rectTransform.sizeDelta.y), m_durationHealthFillAnimation);
        m_fillAnimation.onUpdate = delegate
        {
            m_fill.color = m_gradient.Evaluate(m_healthFillUi);
        };
        m_fillBackAnimation = m_fillBack.rectTransform.DOSizeDelta(new Vector2(m_healthFillUi, m_fillBack.rectTransform.sizeDelta.y), m_durationHealthFillAnimation)
            .SetDelay(m_delayFillBackHealthFillAnimation);
    }

    private void ShakeHealthBar()
    {
        m_shakeHealthBarAnimation = m_rectTransform.DOShakePosition(m_durationShakeHealthBarAnimation, m_strengthShakeHealthBarAnimation,
            m_vibratoShakeHealthBarAnimation);
    }

    private void AnimateHeartAndBarColors()
    {
        m_heart.color = Color.white;
        m_bar.color = Color.white;
        if (m_lowStateActive)
        {
            return;
        }
        int loops = 2;
        if (HealthIsLow())
        {
            loops = -1;
            m_lowStateActive = true;
        }
        m_heartColorAnimation = m_heart.DOColor(m_colorHeartAndBarColorsAnimation, m_durationHeartAndBarColorsAnimation)
            .SetLoops(loops, LoopType.Yoyo);
        m_barColorAnimation = m_bar.DOColor(m_colorHeartAndBarColorsAnimation, m_durationHeartAndBarColorsAnimation)
            .SetLoops(loops, LoopType.Yoyo);
    }

    private bool HealthIsLow()
    {
        return m_healthPercentage <= m_lowHealthPercentageThreshold;
    }
}
