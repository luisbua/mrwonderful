﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;
using System;

public class RaceManager : MonoBehaviour
{
    [SerializeField] private MiniGameInformationSender m_levelRecommendedSender;
    [SerializeField] private bool m_speedBoostUsed;
    [SerializeField] private bool m_jumpBoostUsed;
    [SerializeField] private bool m_shootBoostUsed;
    public BaseBoosterAnimation m_rocketAnimation;
    [SerializeField] private BaseBoosterAnimation m_balloonAnimation;
    [SerializeField] private BaseBoosterAnimation m_chilliAnimation;
    [SerializeField] private AudioClip m_rocketBoosterSelectedSFX;
    [SerializeField] private AudioClip m_balloonBoosterSelectedSFX;
    [SerializeField] private AudioClip m_chilliBoosterSelectedSFX;
    //public GameObject[] Character;
    public GameObject Player;

    public Button SpeedButton;
    public Button JumpButton;
    public Button AttackButton;
    public Button MenuButton;
    public Text SpeedText;
    public Text JumpText;
    public Text ShootText;
    public Text DieText;
    public Image[] TimesLeftBoosters;
    public GameObject[] activateBoostersFX;
    public AudioSource runBSO;
    public GameObject m_runParticles;

    public Vector3[] wayPointsBoosters;

    public bool SpeedBoost = false;
    public bool JumpBoost = false;
    public bool AttackBoost = false;

    [HideInInspector] public int SpeedTimeCount = 0;
    [HideInInspector] public int JumpTimeCount = 0;
    [HideInInspector] public int ShootTimeCount = 0;
    [HideInInspector] public bool IsPlayerDead = false;

    int FinalXP = 0;
    float MaxTimeRaceSec = 60;
    public float BoostSpeed = 30;
    float BoostSpeedTime = 4.5f;
    int BoostJump = 5;
    float BoostJumpTime = 10;
    int BoostAttack = 50;
    float BoostAttackTime = 10;

    float originalFov;
    bool usingSpeedBo;

    GameManagerMWF gm;
    SectionsSpawn sectionSpawn;
    CharacterStats charStats;
    CharacterMovement charMov;
    CharSounds charSounds;
    


    List<ParticleSystem> particlesSpeed = new List<ParticleSystem>();
    List<ParticleSystem> particlesJump = new List<ParticleSystem>();
    List<ParticleSystem> particlesChilli = new List<ParticleSystem>();

    Image fadeIn;
    Tween animationFadeIn;
    TweenCallback callBack;
    TweenCallback downBoosterCallBack;
    // - Test
    public int buaAtack;
    public int buaSalto;

    int index2;

    public GameObject boosterConsumeFX;


    
 
    
    void Start()
    {
        callBack += EndRace;
        charSounds = FindObjectOfType<CharSounds>();

        FinalXP = 0;
        gm = FindObjectOfType<GameManagerMWF>();
        sectionSpawn = FindObjectOfType<SectionsSpawn>();
        originalFov = Camera.main.fieldOfView;
        

        gm.bIsMinGameActive = false;
        //Debug.Log("Bioma: " + gm.Biome.ToString() + " Game: " + gm.Game.ToString() + " GameLevel: " + gm.GameLevel.ToString());

        //simulo la carga desde servidor
        //gm.BoostRun.bIsSpeed = SpeedBoost;
        //gm.BoostRun.bIsJump = JumpBoost;
        //gm.BoostRun.bIsAttack = AttackBoost;

        SpeedButton.gameObject.SetActive(gm.BoostRun.bIsSpeed);
        JumpButton.gameObject.SetActive(gm.BoostRun.bIsJump);
        AttackButton.gameObject.SetActive(gm.BoostRun.bIsAttack);

        GetComponent<SectionsSpawn>().enabled = true;
        GetComponent<Obstacles>().enabled = true;
        GetComponent<CoinSpawn>().enabled = true;
        GetComponent<PowerUpManager>().enabled = true;

        ///Instance Player
        GameObject go;
        int index = (int)gm.Player;
        go = Instantiate(gm.PlayersInfo[index].Character, Player.transform.position, Player.transform.rotation);
        go.transform.parent = Player.transform;
        go.transform.rotation = Quaternion.Euler(Vector3.zero);
        go.GetComponent<Rigidbody>().useGravity = true;
        go.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
        charStats = FindObjectOfType<CharacterStats>();
        charMov = FindObjectOfType<CharacterMovement>();
        //Invoke("TestJump", 1);

        charMov.PlayerDead += PlayerDead;

        fadeIn = GameObject.Find("FadeIn").GetComponent<Image>();

        index2 = index;

        m_levelRecommendedSender.SendRecommendedLevel();

    }

   

    // Update is called once per frame
    void Update()
    {
        if (charSounds == null)
        {
            charSounds = FindObjectOfType<CharSounds>();
        }
        if (usingSpeedBo)
        {
            Camera.main.fieldOfView = Mathf.Lerp(Camera.main.fieldOfView, 135, 3 * Time.deltaTime);
        }
        else
        {
            Camera.main.fieldOfView = Mathf.Lerp(Camera.main.fieldOfView, originalFov, 3 * Time.deltaTime);
        }
    }
    public bool m_usingSpeedBooster;
    public void SpeedButtonPressed()
    {
        m_speedBoostUsed = true;
        m_usingSpeedBooster = true;
        gm.PowerupInfo[0].UnitsLeft--;

        m_rocketAnimation.OnSelected(BoostSpeedTime);
        SingletonSoundPlayer.Instance.m_audioSource.PlayOneShot(m_rocketBoosterSelectedSFX);
        sectionSpawn.SetRaceSpeed(sectionSpawn.RaceSpeed + BoostSpeed);
        //StartCoroutine(StartCountDown((int)BoostSpeedTime,0));

        
        GameObject go = Instantiate(charSounds.runBoosterPS, charSounds.gameObject.transform.position + new Vector3(0, 0.78f, -0.284f), Quaternion.identity);
        go.transform.parent = charSounds.gameObject.transform;
        usingSpeedBo = true;
        foreach (ParticleSystem item in go.GetComponentsInChildren<ParticleSystem>())
        {
            particlesSpeed.Add(item);
        }
        //Invoke("ResetBoostSpeed", BoostSpeedTime);

        float torrentNoTeEncebolles = 0;
        m_speedBoosterTween = DOTween.To(() => torrentNoTeEncebolles, x => torrentNoTeEncebolles = x, BoostSpeedTime, BoostSpeedTime).OnComplete(ResetBoostSpeed);


    }

    public void PauseBoosterSpeed()
    {
        foreach (var item in particlesSpeed)
        {
            item.Stop();
        }
        m_speedBoosterTween.Pause();
        m_rocketAnimation.PauseConsumeTween();

    }
    public void ResumeBoosterSpeed()
    {
        foreach (var item in particlesSpeed)
        {
            item.Play();
        }
        m_speedBoosterTween.Play();
        m_rocketAnimation.ResumeConsumeTween();
    }

    public Tween m_speedBoosterTween;
    public void JumpButtonPressed()
    {
        m_jumpBoostUsed = true;

        gm.PowerupInfo[1].UnitsLeft--;


        SingletonSoundPlayer.Instance.m_audioSource.PlayOneShot(m_balloonBoosterSelectedSFX);
        m_balloonAnimation.OnSelected(BoostJumpTime);

        charMov.Jump += BoostJump;
        StartCoroutine(StartCountDown((int)BoostJumpTime, 1));

        // - Sound
        charSounds.PlaySound(charSounds.m_runBoostersSounds.m_activateJump, 1);

        // - Particle
        charSounds = FindObjectOfType<CharSounds>();
        GameObject go = Instantiate(charSounds.jumpBoosterPS, charSounds.gameObject.transform.position, Quaternion.identity);
        go.transform.parent = charSounds.gameObject.transform;
        charMov.isPowerUpJump = true;
        foreach (ParticleSystem item in go.GetComponentsInChildren<ParticleSystem>())
        {
            particlesJump.Add(item);
        }

        Invoke("ResetBoostJump", BoostJumpTime);
    }
    
    public void AttackButtonPressed()
    {

        m_shootBoostUsed = true;

        gm.PowerupInfo[2].UnitsLeft--;


        //AttackButton.transform.DOLocalRotate(new Vector3(0, 720, 0), 0.5f, RotateMode.FastBeyond360);
        //AttackButton.transform.DOPunchPosition(new Vector3(0, 150, 0), 0.75f, 0, 0, false);

        SingletonSoundPlayer.Instance.m_audioSource.PlayOneShot(m_chilliBoosterSelectedSFX);
        m_chilliAnimation.OnSelected(BoostAttackTime);

        // - Sound
        charSounds.PlaySound(charSounds.m_runBoostersSounds.m_activateShoot, 1);

        // - Particle
        charSounds = FindObjectOfType<CharSounds>();
        GameObject go = Instantiate(charSounds.chilliBoosterPS, charSounds.gameObject.transform.position, Quaternion.identity);
        go.transform.parent = charSounds.gameObject.transform;
        charStats.Attack += BoostAttack;
        charMov.hasProjectilePowerUp = true;
        charMov.StartConstantShoot();
        
        StartCoroutine(StartCountDown((int)BoostAttackTime, 2));

        foreach (ParticleSystem item in go.GetComponentsInChildren<ParticleSystem>())
        {
            particlesChilli.Add(item);
        }

        Invoke("ResetBoostAttack", BoostAttackTime);
    }

    public void ResetBoostSpeed()
    {
        sectionSpawn.ResetSpeed();
        ResetBoostSpeedParticles();
        m_usingSpeedBooster = false;
        //StartCoroutine(WidgetMovement(SpeedButton.gameObject));
    }

    void ResetBoostJump()
    {
        charMov.Jump -= BoostJump;
        JumpText.gameObject.SetActive(false);
        charMov.isPowerUpJump = false;
        foreach (ParticleSystem particle in particlesJump)
        {
            particle.Stop();
        }
        //StartCoroutine(WidgetMovement(JumpButton.gameObject));
    }
    //IEnumerator WidgetMovement(GameObject widget)
    //{
    //    widget.transform.DOMoveY(200, 0.75f);
    //    widget.transform.DOLocalRotate(new Vector3(0, 720, 0), 1, RotateMode.FastBeyond360).SetEase(Ease.InOutElastic);
    //    yield return new WaitForSeconds(1);
    //    widget.transform.DOMoveY(-200, 0.5f);
    //}

    void ResetBoostAttack()
    {
        charStats.Attack -= BoostAttack;
        ShootText.gameObject.SetActive(false);
        charMov.hasProjectilePowerUp = false;

        foreach (ParticleSystem particle in particlesChilli)
        {
            particle.Stop();
        }
        //StartCoroutine(WidgetMovement(AttackButton.gameObject));
    }

    public void CalculateXP()
    {
        int TimeScore = 0;
        int DiamondsScore = 0;
        int LifeScore = 0;
        int BaseScore = 0;

        float ElapsedTimeRace = GetComponent<SectionsSpawn>().ElapsedTimeRace;
        int Score = GetComponent<SectionsSpawn>().Score;
       

        BaseScore = Mathf.RoundToInt(gm.DataXP[gm.GameLevel-1].MinXP);
        TimeScore = Mathf.RoundToInt(CalculatePercentage(20) / ElapsedTimeRace)*10;
        DiamondsScore = Mathf.RoundToInt((Score * 40) / CalculatePercentage(60));
        LifeScore = FindObjectOfType<CharacterStats>().Life;
        if (LifeScore <= 0) LifeScore = 0;
        //FinalXP = (IsPlayerDead) ? BaseScore : BaseScore + TimeScore + DiamondsScore + LifeScore;
        float finalXp = BaseScore + TimeScore + DiamondsScore + (LifeScore);

        gm.BaseScore = BaseScore;
        print("Base score: " + BaseScore);
        gm.TimeScore = TimeScore;
        print("Time score: " + TimeScore);
        gm.PointsScore = Score;
        print("Points score: " + Score);
        gm.LifeScore = LifeScore;
        print("Life score: " + LifeScore);

        print(finalXp);
        Debug.Break();

        float lifeScore = (float) LifeScore;
        float realToughness = (float) charStats.BaseLife;
        //gm.LifeScore = (int) (lifeScore / realToughness * 100);
        gm.TotalXpScore = (int)finalXp;
        gm.IsPlayerDead = IsPlayerDead;
        GameManagerMWF.Trophy lastTrophyObtained = GameManagerMWF.Trophy.Empty;

        if (finalXp >= gm.DataXP[gm.GameLevel - 1].CopperTrophy)
        {
            gm.SpriteLastTrophyObtained = gm.CopperThrophy;
            lastTrophyObtained = GameManagerMWF.Trophy.Copper;
        }
        if (finalXp >= gm.DataXP[gm.GameLevel - 1].SilverTrophy)
        {
            gm.SpriteLastTrophyObtained = gm.SilverThrophy;
            lastTrophyObtained = GameManagerMWF.Trophy.Silver;
        }
        if (finalXp >= gm.DataXP[gm.GameLevel - 1].GoldTrophy)
        {
            gm.SpriteLastTrophyObtained = gm.GoldThrophy;
            lastTrophyObtained = GameManagerMWF.Trophy.Gold;
        }

        GameManagerMWF.DataLevel level = gm.LevelsInfo[(int) gm.Game]
            .DataInfo[gm.GameLevel - 1];
        GameManagerMWF.Trophy minigameTrophyRecord = level.TrophyObtained;
        if ((int) minigameTrophyRecord < (int) lastTrophyObtained)
        {
            gm.LevelsInfo[(int) gm.Game]
                .DataInfo[gm.GameLevel - 1].TrophyObtained = lastTrophyObtained;
        }
        int playerIndex = (int) gm.Player;
        gm.PlayersInfo[playerIndex].XpGained += (int)finalXp;
    }

    public void FadeEndRoad()
    {
        animationFadeIn = fadeIn.DOFade(1, 1).OnComplete(callBack);
        runBSO.DOFade(0, 1);
    }
    void EndRace()
    {
        SceneManager.LoadScene("Podium", LoadSceneMode.Single);

        // - Enviar información de boosters utilizados.
        //gm.SetRunBoosters();
    }


    int CalculatePercentage(float value)
    {
        int n1 = Mathf.RoundToInt(gm.DataXP[gm.GameLevel-1].MaxXP);
        int n2 = Mathf.RoundToInt(gm.DataXP[gm.GameLevel-1].MinXP);
        float baseMax = n1 - n2;
        baseMax = (baseMax * value) / 100;
        return Mathf.RoundToInt(baseMax);
    }

    public void LoadMenu()
    {
        SceneLoadersContainer.Instance.GetSceneLoader("MainSceneLoader").LoadSceneAsync("Mapamundi");
    }

    public void SetCharParameter(int speed, int jump, int attack)
    {
        FindObjectOfType<SectionsSpawn>().InitalSpeedRace(speed);
        FindObjectOfType<CharacterMovement>().Jump = jump;
        FindObjectOfType<CharacterMovement>().Attack = attack;
    }

    IEnumerator StartCountDown(int count, int OptionBoost)
    {
        TimesLeftBoosters[OptionBoost].gameObject.SetActive(true);
        activateBoostersFX[OptionBoost].gameObject.SetActive(true);

        //TimesLeftBoosters[OptionBoost].rectTransform.localScale = Vector3.one;
        //TimesLeftBoosters[OptionBoost].DOFillAmount(0, count);
        //TimesLeftBoosters[OptionBoost].DOColor(Color.red, count);
        //TimesLeftBoosters[OptionBoost].gameObject.transform.DOLocalRotate(new Vector3(0, 720, 0), 0.5f, RotateMode.FastBeyond360);

        for (int i = 0; i < count; i++)
        {
            switch (OptionBoost)
            {
                case 0:
                    SpeedTimeCount = count - i;
                    break;
                case 1:
                    JumpTimeCount = count - i;
                    break;
                case 2:
                    ShootTimeCount = count - i;
                    break;
                default:
                    break;
            }
            yield return new WaitForSeconds(1f);
        }

        if (OptionBoost == 0)
        {
            SpeedButton.interactable = false;
        }
        else if (OptionBoost == 1)
        {
            JumpButton.interactable = false;
        }
        else if (OptionBoost == 2)
        {
            AttackButton.interactable = false;
        }
        TimesLeftBoosters[OptionBoost].gameObject.SetActive(false);
        activateBoostersFX[OptionBoost].GetComponent<ParticleSystem>().Stop();
        yield return new WaitForSeconds(3);
        activateBoostersFX[OptionBoost].gameObject.SetActive(false);

    }

    public void PlayerDead()
    {
        FindObjectOfType<AExperienceCalculator>().UpdateValues();
        Tween fadeInTween = fadeIn.DOFade(1, 1);
        fadeInTween.onComplete = delegate
        {
            fadeInTween.Kill(true);
            SceneManager.LoadScene("EndMiniGame");
        };
        //runBSO.DOFade(0, 1);
    }

    public void ResetBoostSpeedParticles()
    {
        SpeedText.gameObject.SetActive(false);
        usingSpeedBo = false;
        foreach (ParticleSystem particle in particlesSpeed)
        {
            particle.Stop();
        }
    }

    
}
