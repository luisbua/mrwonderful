﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

//!Clase encargada de controlar los aspectos de la aplicación. 
//!@author Luis Buades.
//!@date Marzo 2021.
//!
//!Se mantiene activa en todas las escenas. 
//!Contiene todas las variables globales, funciona como un Singleton.
//!Hace la carga y descarga de datos del servidor.
//!Se ejecuta en la escena Preload

public class GameManagerMWF : MonoBehaviour
{
	[Serializable]
	internal class DatoGet
	{
		public string id;
		public string name;
		public string value;
	}

	///Objeto GameManager
	private GameObject gameManager;

	public enum BiomeType
	{
		Paraiso = 0,
		Guacamole,
		Fantasia,
		Dulce,
		ValleWonder
	}

	public enum GameType
	{
		Run = 0,
		Pang,
		Throw,
		General
	}

	public enum PlayerType
	{
		Aguacate = 0,
		Cupcake,
		Heart,
		Sushi,
		Pina
	}

	public enum Trophy
	{
		Empty = 0,
		Copper,
		Silver,
		Gold
    }

    public enum CrateType
    {
        Basic = 0,
        Good,
        Best
    }

    public enum SkinType
    {
        Sunglasses = 0,
        Hat,
        Lollypop,
        Boots
    }

    public struct RunBoost
	{
		public bool bIsSpeed;
		public bool bIsJump;
		public bool bIsAttack;
	}

	public struct PangBoost
	{
		public bool bIsBattery;
		public bool bIsBomb;
		public bool bIsGem;

	}

	public struct ThrowBoost
	{
		public bool bIsDonut;
		public bool bIsLeaf;
		public bool bIsBolt;
	}

	[System.Serializable]
	public struct LevelProgressInfo
	{
		public int Level;
		public int XpNeeded;
	}

	[System.Serializable]
	public struct PlayerStats
	{
		public int Speed;
		public int Jump;
		public int Attack;
		public int Toughness;
	}

	[System.Serializable]
	public struct CharInfo
	{
		public int Id;
		public Sprite Avatar;
		public GameObject Character;
		public string Name;
		public int Level;
		public bool Locked;
		public int BaseSpeed;
		[HideInInspector] public int RealSpeed;
		public int BaseJump;
		public int RealJump;
		public int BaseAttack;
		public int RealAttack;
		public int BaseToughness;
		public int RealToughness;
		public int XpGained;
		public bool bIsHat;
		public bool bIsGlasses;
		public bool bIsLollypop;
		public bool bIsBoots;
	}

	[System.Serializable]
	public struct PowerUpInfo
	{
		public int Id;
		public string Name;
		public int UnitsLeft;
		public string Description;
		public GameType TypeGame;
	}

	[System.Serializable]
	public struct Dialogue
	{
		public int id;
		public bool ShowDialogue;
		public string WhatIs;
	}

	[System.Serializable]
	public struct XPData
	{
		public int Level;
		public float MaxXP;
		public float MinXP;
		public float CopperTrophy;
		public float SilverTrophy;
		public float GoldTrophy;
	}

	[System.Serializable]
	public struct DataBiome
    {
		public BiomeType Biome;
		public string Name;
		public int neededLevelToUnlock;
		public bool isUnlocked;
    }

	[System.Serializable]
	public struct DataLevel
	{
		public BiomeType Biome;
		public int Level;
		public bool isUnlocked;
		public Trophy TrophyObtained;
	}

	[System.Serializable]
	public struct InfoLevels
	{
		public GameType Game;
		public DataLevel[] DataInfo;
	}

	public struct PowerUpUnits
	{
		public int PowerId;
		public int Units;
	}

	[System.Serializable]
	public struct DataCrate
	{
		public int Id;
		public int Money;
		public List<PowerUpUnits> Boosters;
		public DateTime CreatedTime;
	}

	[Serializable]
	public struct UserInfo
	{
		public int id;
		public string email;
		public string firstname;
		public string lastname;
		public string dob;
		public int gender;
	}

	public bool m_dialogueInScene;
	public bool dialogueINSCENE()
    {
		if (m_dialogueInScene)
        {
			return true;
        }
        else
        {
			return false;
        }
    }

	public IntValue m_appBuildVersion;
	public bool IsUnlockable = false;
	public bool MainGameManager = false;
	public int m_playerLives = 3;

	public int PlayerLives
	{
		get => m_playerLives;
		set
		{
			m_playerLives = value;
			if (m_playerLives < 0)
			{
				m_playerLives = 0;
			}
			else if (m_playerLives > MaxNumPlayerLives)
			{
				m_playerLives = MaxNumPlayerLives;
			}
			LastTimeLivesModified = (int) DateTime.Now.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
		}
	}

	public SaveToServerAdapter m_saveToServerAdapter;
	public int MaxNumPlayerLives = 5;
	public string InitialMap;
	public bool IsPlayerInputDisabled = true;
	public LevelProgressInfo[] LevelsProgressInfo;
	public CharInfo[] PlayersInfo;
	public PowerUpInfo[] PowerupInfo;
	public Dialogue[] ShowDialogues;
	public InfoLevels[] LevelsInfo;
	public DataBiome[] DataBiomes;
 	public XPData[] DataXP;
	public Sprite EmptyThrophy;
	public Sprite CopperThrophy;
	public Sprite SilverThrophy;
	public Sprite GoldThrophy;
	public Sprite EvilCloud;
	public int Money;
	public bool ShowIntroVideo = false;
	public int LastMiniGamePhase;

	[Header("Skins unblock")]
	public bool m_sunglassesSkinUnblock = true;
	public bool m_hatSkinUnblock = true;
	public bool m_lollypopSkinUnblock = true;
	public bool m_bootsSkinUnblock = true;

	public Sprite SpriteLastTrophyObtained;
	public int LastTimeLivesModified;
	[HideInInspector] public List<string> LoadSceneParameters;
	[HideInInspector] public BiomeType Biome;
	[HideInInspector] public GameType Game;
	public int GameLevel;
	[HideInInspector] public PlayerType Player;
	[HideInInspector] public RunBoost BoostRun;
	[HideInInspector] public PangBoost BoostPang;
	[HideInInspector] public ThrowBoost BoostThrow;
	[HideInInspector] public bool bIsMinGameActive;
	[HideInInspector] public DateTime LastConnection;
	[HideInInspector] public int LifeRespawnTime;
	[HideInInspector] public bool isFirstTime = true;
	[HideInInspector] public bool mustSelectFirstCharacter = true;
	[HideInInspector] public bool isInitMiniGame = false;
	[HideInInspector] public int SelectedCharId = 0;
	[HideInInspector] public int BaseScore;
	[HideInInspector] public int TimeScore;
	[HideInInspector] public int TimeElapsed;
	[HideInInspector] public int LifeScore;
	[HideInInspector] public int LifeRemained;
	[HideInInspector] public int PointsScore;
	[HideInInspector] public int RealPoints;
	[HideInInspector] public int TotalXpScore;
	[HideInInspector] public string LastThrowWonLevel;
	[HideInInspector] public bool IsPlayerDead;
	[HideInInspector] public int[] OrderToUnblock = { 0, 4, 2, 1, 3 };
	[HideInInspector] public DataCrate[] CrateData;
	[HideInInspector] public bool hasFirstCharacter;
	[HideInInspector] public bool m_lifesTutorialShowed;
	[HideInInspector] public bool m_movementTutorialShowed;
	[HideInInspector] public int m_lastMiniGameLevelRecommended;
	[HideInInspector] public string pangLevelWon;
	public UserInfo userInfo;

	public string m_apiRootUrl = "https://mrwon.easyfresh.tk/MrWonPla/public/index.php/api/";
	private float ActualTime;
	private float ElapsedTime;
	private float DelayTimeSeconds = 3;
	public bool FirstDialogueDone;
	public bool PlayingAsGuest = true;
	public ApplicationVersionChecker m_applicationVersionChecker;

	[SerializeField] private float m_lifeRespawnInSeconds = 300;
	public float m_lifeRespawnTimer;

	private DateTime buaTime;

	public void UpdatePlayerLifes()
	{
		LifesBar lifesBar = FindObjectOfType<LifesBar>();
		if (lifesBar != null)
		{
			lifesBar.SetHearts();
		}
	}

    void Start()
	{
		Application.targetFrameRate = 300;
	
		Screen.autorotateToLandscapeLeft = false;
		Screen.autorotateToLandscapeRight = false;
		Screen.autorotateToPortrait = true;
		Screen.autorotateToPortraitUpsideDown = true;

		///Busco el objeto llamado GameManager
		GameObject gameManager = GameObject.Find("GameManager");

		///Le indico que no se destruya al cargar otra escena 
		DontDestroyOnLoad(gameManager);

		LoadInitialSceneDependingOnAppVersion();

		m_saveToServerAdapter.OnDataLoaded += delegate
		{
			StartCoroutine(CalculateLivesAccordingToWaitedTime());
		};
		
		CalculateRealStats();

		LoadSceneParameters = new List<string>();
		LoadSceneParameters.Add("0");
	}

    private void LoadInitialSceneDependingOnAppVersion()
    {
	    StartCoroutine(DoLoadInitialSceneDependingOnAppVersion());
    }

	private IEnumerator DoLoadInitialSceneDependingOnAppVersion() // Comprobar que la versión de la aplicación se corresponde con la última versión en la API.
	{
		UnityWebRequest apiGetterRequest = UnityWebRequest.Get("https://mrwon.quasardynamics.com/api_url_getter.php");
		yield return apiGetterRequest.SendWebRequest();
		if (apiGetterRequest.error != null)
		{
			AdviseInternetNeeded();
		}
		else
		{
			m_apiRootUrl = apiGetterRequest.downloadHandler.text;
			UnityWebRequest lastStableVersionRequest = UnityWebRequest.Get(m_apiRootUrl + "Ajustes/last_stable_version");
			yield return lastStableVersionRequest.SendWebRequest();
			if (lastStableVersionRequest.error != null)
			{
				AdviseInternetNeeded();
			}
			else
			{
				DatoGet lastStableVersionDato = JsonUtility.FromJson<DatoGet>(lastStableVersionRequest.downloadHandler.text);
				int lastStableVersion = int.Parse(lastStableVersionDato.value);
				if (VersionIsObsolete(lastStableVersion))
				{
					SceneLoadersContainer.Instance.GetSceneLoader("MainSceneLoader").LoadSceneAsync("ObsoleteVersion");
				}
				else
				{
					SceneLoadersContainer.Instance.GetSceneLoader("MainSceneLoader").LoadSceneAsync(InitialMap);
				}
			}
		}
	}

	private void AdviseInternetNeeded()
	{
		AbstractInformer.Instance
			.Show(
				"Necesitas estar conectado a Internet para poder acceder a la aplicación. Conéctate y luego reinténtalo.")
			.WithConfirmButton("Reintentar", LoadInitialSceneDependingOnAppVersion)
			.WithCloseButton(Application.Quit);
	}

	private bool VersionIsObsolete(int lastStableVersion)
	{
		return lastStableVersion > m_appBuildVersion.m_value;
	}

	void Update()
	{
		if (PlayerLives < MaxNumPlayerLives)
        {
			LifeRespawner();
        }
        else
        {
			m_lifeRespawnTimer = m_lifeRespawnInSeconds;
        }
	}

	private IEnumerator CalculateLivesAccordingToWaitedTime()
	{
		yield return null;

		LastConnection = new DateTime(1970,1,1,0,0,0,0,System.DateTimeKind.Utc);
		LastConnection = LastConnection.AddSeconds( LastTimeLivesModified ).ToLocalTime();
		
		int secondsOfDifference = Mathf.Abs((int) (DateTime.Now - LastConnection).TotalSeconds);
		int amountOfLivesToAdd = secondsOfDifference / (int) m_lifeRespawnInSeconds;
		int remainingSeconds = secondsOfDifference % (int) m_lifeRespawnInSeconds;
		m_lifeRespawnTimer -= remainingSeconds;

		PlayerLives += amountOfLivesToAdd;
	}

	private void LifeRespawner()
    {
		m_lifeRespawnTimer -= Time.deltaTime;
        if (m_lifeRespawnTimer <= 0)
        {
			m_lifeRespawnTimer = m_lifeRespawnInSeconds;
			PlayerLives++;
			UpdatePlayerLifes();
		}
	}

	/*!
	* Funcion para quitar o poner vidas.
	* @param[in]  AddLife  Bool para indicar si quito o pongo.
	*/
	public void SetLives(bool AddLife)
	{
		if (PlayerLives >= 0 && PlayerLives < MaxNumPlayerLives)
		{
			PlayerLives = (AddLife) ? ++PlayerLives : --PlayerLives;
			if (FindObjectOfType<LifesBar>()) FindObjectOfType<LifesBar>().SetHearts();
		}
	}

	public void CalculateRealStats()
	{
		for (int i = 0; i < PlayersInfo.Length; i++)
		{
			PlayersInfo[i].RealSpeed = PlayersInfo[i].BaseSpeed * (PlayersInfo[i].Level + 1);
			PlayersInfo[i].RealJump = PlayersInfo[i].BaseJump * (PlayersInfo[i].Level + 1);
			PlayersInfo[i].RealAttack = PlayersInfo[i].BaseAttack * (PlayersInfo[i].Level + 1);
			PlayersInfo[i].RealToughness = PlayersInfo[i].BaseToughness * (PlayersInfo[i].Level + 1);
		}
	}

	public void SelectFirstCharacter()
	{
		mustSelectFirstCharacter = false;
		SceneLoadersContainer.Instance.GetSceneLoader("SpinnerSceneLoader").LoadSceneAsync("FirstCharacter");
	}

	public void UnlockBiome(BiomeType biomeType)
	{
		var dataBiome = DataBiomes[(int) biomeType];
		dataBiome.isUnlocked = true;
	}

	public int TranslateXpToLevel()
	{
		bool found = false;
		int level = 0;
		int playerIndex = (int) Player;
		int playerXp = PlayersInfo[playerIndex].XpGained;
		for (int i = 0; i < LevelsProgressInfo.Length && !found; i++)
		{
			if (LevelsProgressInfo[i].XpNeeded <= playerXp)
			{
				level = LevelsProgressInfo[i].Level;
			}
			else
			{
				found = true;
			}
		}

		return level;
	}

	public int GetCharactersMaximumLevel()
	{
		int level = 0;
		foreach (var playerInfo in PlayersInfo)
		{
			if (playerInfo.Level > level && !playerInfo.Locked)
			{
				level = playerInfo.Level;
			}
		}
		return level;
	}

	public void SaveUserInfo(string jsonToRead)
	{
		userInfo = JsonUtility.FromJson<UserInfo>(jsonToRead);
	}

	public void ResetAllValues()
	{
		for (int i = 0; i < PlayersInfo.Length; i++)
		{
			PlayersInfo[i].Locked = true;
			PlayersInfo[i].Level = 0;
			PlayersInfo[i].XpGained = 0;
			PlayersInfo[i].bIsHat = false;
			PlayersInfo[i].bIsGlasses = false;
			PlayersInfo[i].bIsLollypop = false;
			PlayersInfo[i].bIsBoots = false;
		}

		for (int i = 0; i < PowerupInfo.Length; i++)
		{
			PowerupInfo[i].UnitsLeft = 1;
		}

		for (int i = 0; i < LevelsInfo.Length; i++)
		{
			for (int j = 0; j < LevelsInfo[i].DataInfo.Length; j++)
			{
				if (LevelsInfo[i].DataInfo[j].Biome != BiomeType.ValleWonder) // No se cuenta Valle Wonder
				{
					LevelsInfo[i].DataInfo[j].isUnlocked = false;
					LevelsInfo[i].DataInfo[j].TrophyObtained = Trophy.Empty;
				}
			}
		}

		for (int i = 0; i < DataBiomes.Length - 1; i++) // No se cuenta Valle Wonder
		{
			DataBiomes[i].isUnlocked = false;
		}

		m_hatSkinUnblock = false;
		m_sunglassesSkinUnblock = false;
		m_lollypopSkinUnblock = false;
		m_bootsSkinUnblock = false;

		for (int i = 0; i < ShowDialogues.Length; i++)
		{
			ShowDialogues[i].ShowDialogue = true;
		}

		PlayerLives = 3;
		LastTimeLivesModified = 0;
		Money = 0;
		mustSelectFirstCharacter = true;
		FirstDialogueDone = false;
	}

}